public class Basics{
	public static void main(String[] args){
		System.out.println(17);
		System.out.println(17L);
		System.out.println(17.);
		System.out.println(17.F);
		System.out.println(17.D);
		System.out.println("17");
		//System.out.println('17');
		System.out.println("\"17\""); 
		System.out.println("Printing Character:" + 'A');

		// Creating variables
		int age = 17;
		double length = 17.;
		String code = "17";
		System.out.println(age);
		System.out.println(length);
		System.out.println(code);

		// Math Operators
		System.out.println(3/2);
		System.out.println(3/2.0);
		System.out.println(3.0/2);
		System.out.println("3/2 = " + 3/2);
		System.out.println("3/2 = " + 3/2.0);

		// Augmented assignment operator
		int i = 10;
		int newNum = 10 * i++;
		System.out.println("i is " + i + 
						   ", newNum is " + newNum);
		i = 10;
		newNum = 10 * (++i);
		System.out.println("i is " + i + 
						   ", newNum is " + newNum);

		// Type casting
		System.out.println((double) 3 / 2);
		System.out.println(3 / (double) 2);
		System.out.println((int) (3 / 2.0));

		// Converting String to Numbers
		System.out.println(Integer.parseInt("17"));
		System.out.println(Double.parseDouble("17"));
	}
}