public class TestLib{
	public static void main(String[] args){

		// trigonometric methods
		System.out.println("sin(pi/2) = " + Math.sin(Math.PI/2));
		System.out.println("cos(pi/2) = " + Math.cos(Math.PI/2));
		System.out.println("tan(pi/2) = " + Math.tan(Math.PI/2));
		System.out.println("asin(1.0) = " + Math.asin(1.0));
		System.out.println("acos(0) = " + Math.acos(0));
		System.out.println("atan(0) = " + Math.atan(0));
		System.out.println("toRadians(90) = " + Math.toRadians(90));
		System.out.println("toDegrees(pi/2) = " + Math.toDegrees(Math.PI/2));

		// exponent methods
		System.out.println("exp(1) = " + Math.exp(1));
		System.out.println("log(e) = " + Math.log(Math.E));
		System.out.println("log10(10) = " + Math.log10(10));
		System.out.println("pow(2, 3) = " + Math.pow(2, 3));
		System.out.println("sqrt(16) = " + Math.sqrt(16));

		// rounding methods
		System.out.println("ceil(2.1) = " + Math.ceil(2.1));
		System.out.println("floor(2.1) = " + Math.floor(2.1));
		System.out.println("rint(2.1) = " + Math.rint(2.1));
		System.out.println("round(2.1) = " + Math.round(2.1));

		// min, max, abs
		System.out.println("min(2, 3) = " + Math.min(2, 3));
		System.out.println("max(2, 3) = " + Math.max(2, 3));
		System.out.println("abs(-2.3) = " + Math.abs(-2.3));

		// random
		System.out.println("random() from 0 to 9 = " + Math.random() * 10);

	}
}