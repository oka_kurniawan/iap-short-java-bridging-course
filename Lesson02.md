# Lesson 2: Loops

## Learning Objectives

By the end of this lesson, students should be able to:
- write loops using while and for statements
- differentiate and use two kinds of for statements
- trace Java code containing loops
- create arrays and traverse arrays using loops
- copy, sort, and compare arrays

Iteration or Loops is one of the important basic structure in programming. Java provides more statements to do loops as compared to Python for various reasons. We will start with the `while` statement.

## While Statement

Python provides two statements to do loops. One of them is the `while` statement. 

```python
# python

# initialize counter
count = 0

# while statement and condition
while count < 10:
	# block A, what we want to repeat
	print(count)
	# block B, condition modifier
	count += 1
# block C, next statement
print("This is the next statement")
```

There are several parts of the above while loop codes:
- part 1: initialize counter. This counter will be used in the condition and will be modified in block B.
- part 2: while statement and its condition. The counter is used here as part of the condition. As long as the condition is true, both block A and block B will be executed.
- part 3: block A. This is the statements that you want to repeat as long as the condition is true.
- part 4: block B. This is repeated as long as the condition is true, and you need this block to modify the condition. Failing to modify the condition may result in an infinite loops, which means that your program will never stop.
- part 5: block C. This is not part of the while statement and in fact actually this is the next statement after the while block finishes.

Java also provides the `while` statement and it is usually contains similar parts as above.

```java
// java

// initialize counter
int count = 10;

// while statement and condition
while (count < 10){
	// block A
	System.out.println(count);
	// block B
	count++;
}
// block C
System.out.println("This is the next statement");
```

Notice the following:
- You need to declare the type of the variable in Java when you initialize the counter.
- The condition after the `while` statement requires parenthesis `()`.
- The blocks that will be repeated as long as the condition is true is enclosed with the curly braces `{}`.
- All statements must end with a semicolon `;`. 

## Do-While Loop

Java provides another statement that Python does not have. It is called the `do-while` statement. We can rewrite the above codes as follows:

```java
// java

int count = 10;

do{
	// block A
	System.out.println(count);
	// block B
	count++;
} while (count < 10);
// block C
System.out.println("This is the next statement");
```

There is a big difference between while statement and do-while statement.
- In the `while` statement, the condition is checked before the blocks are executed. The blocks are executed only when the condition is true.
- In the `do-while` statement, the blocks A and B are executed at least **once** regardless if the condition is true or not. 

When is do-while statement useful? Let's see one example where it maybe useful. Let's write a program to enter number to sum positive numbers and stop when it sees a negative one being entered.

```java
// java

import java.util.Scanner;

public class SumDoWhile{
	public static void main(String[] args){
		int data;
		int sum = 0;

		Scanner input = new Scanner(System.in);

		do{
			System.out.print("Enter a positive number to add, end with -1: ");
			data = input.nextInt();
			sum += data;
		} while (data != -1);

		sum++;
		System.out.println("The sum is " + sum);

	}
}
```

Note that in this case, we want at least to get the input for data once before checking the condition `data != -1`. In this case a do-while statement is useful. We can write the same using a while statement, but it will take us to repeat the input two times.
	
```java
// java
import java.util.Scanner;

public class SumWhile{
	public static void main(String[] args){
		int data;
		int sum = 0;

		Scanner input = new Scanner(System.in);

		System.out.print("Enter a positive number to add, end with -1: ");
		data = input.nextInt();

		while (data != -1){
			sum += data;

			System.out.print("Enter a positive number to add, end with -1: ");
			data = input.nextInt();
		} 

		System.out.println("The sum is " + sum);

	}
}
```
Note the folllowing:
- We need to repeat the prompt text and the `nextInt()` function two times when using the while statement. The reason is that `data` must have some values before it can be checked in the condition.
- We have swapped the sequence inside the block. We sum the data first, i.e. `sum += data`, and after that we prompt the user for the input. Recall that in the previous code we put the `sum += data` at the end of the block instead.
- Notice that in the previous code we have `sum++` whereas we do not have it here. The reason is that in the previous code, we added the `-1` value which is used to stop the loop into the sum. To remove that `-1`, we add the `sum` by one at the end of the loop. On the other hand, we need not do that in the last code. This is because the last summation is when the data is not `-1`. When the data is `-1` it will exit the loop before it is added to the sum.

## Arrays

We normally use loops when dealing with collection data type. In Python, List is the most commonly used collection data type. Java has a more primitive data type like Python's list called an *array*. There are several differences between Python's list and Java's array.
- Java's array must be of the same type. Python's list can contain a mixture of data type. Java's array on the other hand must have the same type for its elements. 
- You need to specify the size of the array when you declare it. This is not necessary with Python's list. This is because Java's array is *fixed size*. Once you create an array with 4 elements, you cannot change its size. Declaring the size helps Java to manage the memory size more efficiently. 

If you need something that is more like Python's list, you should look at `java.util.List`. This `List` data type is more similar to Python's List data type. Just take note that Java's array is more primitive and is different from Python's List. Python does not have a fixed size array as in Java.

### Creating Array

To declare an array, we use the following syntax:

```java
// java

elementType[] arrayVar;
```
For example, 
```java
// java
int[] numbers;
```
This tells Java that `numbers` is an array and its element has `int` as its data type. Now, if you recall that in every `main` method of your Java program you have `String[] args` in the argument.

```java
// java
public static void main(String[] args){

}
```
This means that the data type of the argument of the `main` method is an array of `String` elements. 

Second, we need to specify the size of the array.

```java
// java
arrayVar = new elementType[size];
```

For example, if we want to have maximum of 100 numbers, we can set the size as
```java
// java
numbers = new int[100];
```

You can actually do these two steps *declaration* and *creating* arrays into one singgle line.

```java
// java

int[] numbers = new int[100];
```

If you have the actual elements from the beginning, you need not specify the size and can create the array using the following:
```java
// java
int[] numbers = {1, 2, 3, 4, 5};
```
Java knows that the size that needs to be allocated is 5. 

### Accessing Array's Element

Similar to Python's list, you access array's element using the square bracket `[]` operator.

```java
arrayVar[index] = value;
```

For example, you set the third element to 0.
```java
// java
numbers[2] = 0;
```
Notice that the index starts from 0 just like Python's list. You can also print the values using the same square bracket operators.

```java
// java
System.out.println(numbers[2]);
```

### Array's Length

We can get the length of array using the dot operator `.length`. For example,

```java
// java
System.out.println(numbers.length);
```
will print out the length of `numbers` array.

## For-Each Loops

To iterate every element in the list, we use `for-in` statement in Python. Recall in Python we can have the following code:

```python
# python
numbers = [1, 2, 3, 4, 5]
for element in numbers:
	print(element)
```

Java has a similar syntax to do this.

```java
// java
int[] numbers = {1, 2, 3, 4, 5}
for (int element: numbers){
	System.out.println(element);
}
```

## For-Loop

Sometimes, the for-loop in python uses the `range()` function. This is mainly used to count the iteration similar to while loop. For example,

```python
# python
for count in range(10):
	print(count)
```
Such for-loop is equivalent to the following while-loop.
```python
# python

# counter variable
count = 0
# condition
while count < 10:
	# block A, repeated statements
	print(count)
	# condition modifier
	count += 1
```

In Java, you can have a for-loop that is similar to this. It has the following parts:
- counter variable
- condition
- repeated statements inside the loop
- condition modifier

```java
// java
for (counterVar = initValue; condition; conditionModifer){
	// block A, repeated statements
}
```

For example, to do something similar as Python's code above, we write the following:
```java
// java
for (int count = 0; count < 10; count++){
	System.out.println(count);
}
```
Note that:
- The count is initialized to 0. You may need to declare it first with the respective data type.
- The second part in the parenthesis is the condition when the loop is to be executed.
- The last part is the condition modifier. 

In this way, we can also traverse an array or list using this kind of loop. In Python, we will write it this way:

```python
# python
numbers = [1, 2, 3, 4, 5]
for idx in range(len(numbers)):
	print(numbers[idx])
```

We can write this kind of loop in Java as follows:

```java
// java
int[] numbers = {1, 2, 3, 4, 5};

for (int idx = 0; idx < numbers.length; idx++){
	System.out.println(numbers[idx]);
}
```

Note that if you print `idx` after the loop, you will get an error. The reason is that `idx` is declared inside the loop only. The scope of the variable is limited to the body of the loop. If you need to use `idx` outside of the loop, you need to declare it outside of the loop as follows.

```java
// java
int[] numbers = {1, 2, 3, 4, 5};

int idx; // declared outside of the loop

for (idx = 0; idx < numbers.length; idx++){
	System.out.println(numbers[idx]);
}
```

It is better to declare `idx` inside the loop if you don't use it anywhere else as it reduces the possibility of bugs. In general you should declare the variable only within the scope that you need it.

## More on Java's Arrays

Since loops generally involve arrays, it is important to know how to work with arrays. The following sections show some particular way to handle Java's array.

### Copying Arrays

Similar to Python's list, array's name is a reference that points to the content of the array. The assignment operator **do not copy** the elements. 

```java
// java

list2 = list1;
```

This statement *does not copy* the contents of the array, but instead merely copies the reference value from `list1` to `list2`. Changing the element in one of the array will be reflected on the other. For example,

```java
// java
list1[2] = 0;

printArray(list1);
printArray(list2);
```

The above code modify the third element of `list1`. But as seen on the printed screen, the element of `list2` also changes. We have created a method `printArray()` to print the array. See [CopyArray.java](./Lesson02/CopyArray.java).

In order to make a copy of an array, you can either use loops or use `arraycopy`. This is how you can copy using loops:

```java
//java
int[] list3 = new int[list1.length];

for (int idx = 0; idx < list1.length; idx++){
	list3[idx] = list1[idx];
}
```

To use `arraycopy`, you need to use the following syntax:

```java
// java
System.arraycopy(sourceArray, src_pos, targetArray, tar_pos, length);
```
Notice that `arraycopy` is provided by `java.lang.System` class.

For example to copy `list1` to `list3` using `arraycopy` we write:

```java
// java
int[] list3 = new int[list1.length];

System.arraycopy(list1, 0, list3, 0, list1.length);
```

Note that you can use this to slice array to create a new sub-array from the existing array.

### Sorting Arrays

To sort arrays, you can use `java.util.Arrays` class and its `sort()` method. 

```java
// java

Arrays.sort(numbers);
```

Note that this method sort the array in place. This means that your `numbers` variable will be changed after calling `sort()`. 

### Passing Arrays to Methods

Similar to Python, when you pass an array to a method, the reference of the array is passed to the method. This means that if you modify the element of the array inside the method, the changes will remain after you exit the method.

```java
// java
public class PassArrayMethod{
	public static void main(String[] args){
		int x = 1;
		int[] y = new int[10];

		modify(x, y);

		System.out.println(x);
		System.out.println(y[0]);

	}

	public static void modify(int number, int[] numbers){
		number = 1001;
		numbers[0] = 555;
	}
}
```

As shown in the output, `x` is not modified after the call, but `y` is.

### Checking Equality

You can use `equals()` method to check if two arrays are equal.

```java
// java

import java.util.Arrays;

boolean list12 = Arrays.equals(list1, list2); 
boolean list23 = Arrays.equals(list2, list3); 
```


