public class SumArrays{
	public static void main(String[] args){

		int[] numbers = {1, 2, 3, 4, 5, 6, 7, 8, 9};

		//System.out.println(sumUsingForLoop(numbers));
		assert sumUsingForLoop(numbers) == 45
	}

	public static int sumUsingForLoop(int[] numbers){
		int sum = 0;

		for (int item: numbers){
			sum += item;
		}

		return sum;
	}

	public static int sumUsingRecursion(int[] numbers){
		if numbers.length == 1{
			return numbers[0];
		} else{
			return numbers[0] + sumUsingRecursion(numbers);
		}
	}
}