import java.util.Arrays;	

public class SortArrays{
	public static void main(String[] args){
		double[] numbers = {6.0, 4.1, 1.8, 2.9, 3.2, 3.5};
		double[] tempArrays = new double[numbers.length];

		// using sort default method

		System.arraycopy(numbers, 0, tempArrays, 0, numbers.length);
		Arrays.sort(tempArrays);
		printArray(tempArrays);


	}

	public static void printArray(double[] numbers){
		String result = "[";
		for (double item: numbers){
			result += Double.toString(item) + ", ";
		}
		result = result.substring(0, result.length()-2) + "]";
		System.out.println(result);
	}
}