public class TestWhileLoops{
	public static void main(String[] args){
		// while loop example
		System.out.println("While loop example:");

		int count = 0;
		while (count < 10){
			System.out.println(count);
			count++;
		}
		System.out.println("This is the next statement");
		System.out.println("=====");

		// do-while example
		System.out.println("Do-While example:");

		count = 0;
		do{
			System.out.println(count);
			count++;
		} while (count < 10);
		System.out.println("This is the next statement");
	}
}