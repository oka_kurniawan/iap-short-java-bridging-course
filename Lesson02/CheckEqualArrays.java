import java.util.Arrays;

public class CheckEqualArrays{
	public static void main(String[] args){
		int[] list1 = {2, 4, 7, 10};
		int[] list2 = {2, 4, 7, 10};
		int[] list3 = {5, 4, 7, 10};

		boolean list12 = Arrays.equals(list1, list2); 
		boolean list23 = Arrays.equals(list2, list3); 

		System.out.println(list12);
		System.out.println(list23);

	}
}