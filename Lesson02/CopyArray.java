public class CopyArray{
	public static void main(String[] args){
		int[] list1 = {1, 2, 3, 4, 5};
		int[] list2 = list1;
		int[] list3 = new int[list1.length];

		// example of aliasing

		list1[2] = 0;

		System.out.println(list1);
		System.out.println(list2);

		printArray(list1);
		printArray(list2);

		// copying using for loop

		for (int idx = 0; idx < list1.length; idx++){
			list3[idx] = list1[idx];
		}

		list1[2] = 3;

		printArray(list1);
		printArray(list2);
		printArray(list3);

		// copying using arraycopy

		System.arraycopy(list1, 0, list3, 0, list1.length);
		list1[2] = -1;
		
		printArray(list1);
		printArray(list2);
		printArray(list3);

	}

	public static void printArray(int[] numbers){
		String result = "[";
		for (int item: numbers){
			result += Integer.toString(item) + ", ";
		}
		result = result.substring(0, result.length()-2) + "]";
		System.out.println(result);
	}
}